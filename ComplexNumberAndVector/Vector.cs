﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexNumberAndVector
{
    public class Vector<T> : ICloneable, IComparable<Vector<T>> where T : new()
    {
        List<T> points;

        public Vector()
        {
            points = new List<T>();
        }

        public Vector(List<T> p)
        {
            points = new List<T>(p);
        }

        public Vector(Vector<T> v)
        {
            points = new List<T>(v.Points);
        }

        public static double Module(Vector<T> c1)
        {
            if (typeof(T) == typeof(int) || typeof(T) == typeof(Double))
            {
                double sum = 0;
                foreach (T p in c1.Points)
                    sum += Math.Pow((dynamic)p, 2);
                return Math.Sqrt(sum);
            }
            throw new Exception("no realisation :c");
        }

        public static Vector<T> operator +(Vector<T> c1, Vector<T> c2)
        {
            return new Vector<T>((c1.Points.Zip(c2.Points, (f, s) => Sum(f, s)).ToList<T>()));
        }
        public static Vector<T> operator -(Vector<T> c1, Vector<T> c2)
        {
            return new Vector<T>((c1.Points.Zip(c2.Points, (f, s) => Diff(f, s)).ToList<T>()));
        }
        public static Vector<T> operator *(double d, Vector<T> c2)
        {
            return new Vector<T>((c2.Points.Select((f) => Multi(d, f)).ToList<T>()));
        }
        public static T Skalar(Vector<T> c1, Vector<T> c2)
        {
            List<T> multies = (c1.Points.Zip(c2.Points, (f, s) => Multi(f, s)).ToList<T>());
            return multies.Aggregate((work, next) => Sum(work, next));
        }

        public static Vector<T> Proj(Vector<T> c1, Vector<T> c2)
        {
            return Multi((dynamic)Skalar(c1, c2) / Skalar(c2, c2), Module(c2));
        }

        public static IEnumerable<Vector<T>> Orto(IEnumerable<Vector<T>> vectors)
        {
            List<Vector<T>> results = new List<Vector<T>>();
            if (vectors.ToList<Vector<T>>().Count == 0) return results.AsEnumerable<Vector<T>>();

            int i = 0;

            foreach (Vector<T> vector in vectors)
            {
                if (i == 0)
                    results.Add(vector);
                else
                {
                    Vector<T> res = vector;

                    foreach (Vector<T> vectorNew in results)
                    {
                        res -= Proj(vector, vectorNew);
                        results.Add(res);
                    }
                }
            }

            return results.AsEnumerable<Vector<T>>();
        }





        private static T Sum(dynamic a, dynamic b)
        {
            if (a == null && b != null) return b;
            else if (a != null && b == null) return a;
            else if (a == null && b == null) return default(T);
            return a + b;
        }
        private static T Diff(dynamic a, dynamic b)
        {
            if (a == null && b != null) return -b;
            else if (a != null && b == null) return a;
            else if (a == null && b == null) return default(T);
            return a - b;
        }
        private static T Multi(dynamic a, dynamic b)
        {
            if (a == null || b == null) return default(T);
            return a * b;
        }

        public object Clone()
        {
            return new Vector<T>(this);
        }

        public int CompareTo(Vector<T> other)
        {
            return Comparer<List<T>>.Default.Compare(Points, other.Points);
        }

        public List<T> Points
        {
            get { return points; }
            set { points = value; }
        }

        public override string ToString()
        {
            return "(" + String.Join(" ; ", Points.ToArray<T>()) + ")";
        }
    }
}

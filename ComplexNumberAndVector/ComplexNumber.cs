﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexNumberAndVector
{
    /// <summary>
    /// Class of complex number
    /// </summary>
    class ComplexNumber
    {
        double real, imagine;
        public delegate void MethodContainer();
        public static event MethodContainer onNull;

        /// <summary>
        /// Constructor
        /// </summary>
        public ComplexNumber()
        {
            Re = 0;
            Im = 0;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="r">real part</param>
        /// <param name="i">imagine part</param>
        public ComplexNumber(double r = 1, double i = 0)
        {
            Re = r;
            Im = i;
        }

        /// <summary>
        /// Constructor from another number
        /// </summary>
        /// <param name="num">another complex number</param>
        public ComplexNumber(ComplexNumber num)
        {
            Re = num.Re;
            Im = num.Im;
        }

        /// <summary>
        /// ToString method override
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Im > 0) return String.Format("{0}+{1}i", Re, Im);
            else if (Im == 0) return String.Format("{0}", Re);
            return String.Format("{0}{1}i", Re, Im);
        }

        /// <summary>
        /// Overload operator+
        /// </summary>
        /// <param name="n1">first number</param>
        /// <param name="n2">second number</param>
        /// <returns>
        /// Sum of 2 numbers
        /// </returns>
        public static ComplexNumber operator +(ComplexNumber n1, ComplexNumber n2)
        {
            return new ComplexNumber(n1.Re + n2.Re, n1.Im + n2.Im);
        }

        /// <summary>
        /// Overload operator-
        /// </summary>
        /// <param name="n1">number</param>
        /// <returns>
        /// Negative of number
        /// </returns>
        public static ComplexNumber operator -(ComplexNumber n1)
        {
            return new ComplexNumber(-n1.Re, -n1.Im);
        }

        /// <summary>
        /// Overload operator-
        /// </summary>
        /// <param name="n1">first number</param>
        /// <param name="n2">second number</param>
        /// <returns>
        /// Diff of 2 numbers
        /// </returns>
        public static ComplexNumber operator -(ComplexNumber n1, ComplexNumber n2)
        {
            return new ComplexNumber(n1.Re - n2.Re, n1.Im - n2.Im);
        }

        /// <summary>
        /// Overload operator*
        /// </summary>
        /// <param name="n1">first number</param>
        /// <param name="n2">second number</param>
        /// <returns>
        /// The product of numbers
        /// </returns>
        public static ComplexNumber operator *(ComplexNumber n1, ComplexNumber n2)
        {
            return new ComplexNumber(n1.Re * n2.Re - n1.Im * n2.Im, n1.Im * n2.Re + n1.Re * n2.Im);
        }

        /// <summary>
        /// Overload operator/
        /// </summary>
        /// <param name="n1">first number</param>
        /// <param name="n2">second number</param>
        /// <returns>
        /// A private of two numbers
        /// </returns>
        public static ComplexNumber operator /(ComplexNumber n1, ComplexNumber n2)
        {
            if (n2.Re == 0 && n2.Im == 0) onNull();
            double newReal = (n1.Re * n2.Re + n1.Im * n2.Im) / (n2.Re * n2.Re + n2.Im * n2.Im);
            double newImagine = (n1.Im * n2.Re - n1.Re * n2.Im) / (n2.Re * n2.Re + n2.Im * n2.Im);
            return new ComplexNumber(newReal, newImagine);
        }

        /// <summary>
        /// Module of complex number
        /// </summary>
        /// <param name="n1">number</param>
        /// <returns>Module</returns>
        public static double Module(ComplexNumber n1)
        {
            return Math.Sqrt(n1.Re * n1.Re + n1.Im * n1.Im);
        }

        /// <summary>
        /// Number in pow
        /// </summary>
        /// <param name="n1">number</param>
        /// <param name="num">pow</param>
        /// <returns>ComplexNumber in pow</returns>
        public static ComplexNumber Pow(ComplexNumber n1, double num)
        {
            double module = Module(n1);
            double varphi = 0;
            if (n1.Re != 0) varphi = Math.Acos(n1.Re / module);
            else varphi = Math.Asin(n1.Im / module);
            return new ComplexNumber(Math.Pow(module, num) * Math.Cos(num * varphi), Math.Pow(module, num) * Math.Sin(num * varphi));
        }

        /// <summary>
        /// Root of number
        /// </summary>
        /// <param name="n1">number</param>
        /// <param name="num">root</param>
        /// <returns>Root of ComplexNumber</returns>
        public static ComplexNumber Root(ComplexNumber n1, double num)
        {
            double module = Module(n1);
            double varphi = 0;
            if (n1.Re != 0) varphi = Math.Acos(n1.Re / module);
            else varphi = Math.Asin(n1.Im / module);
            return new ComplexNumber(Math.Pow(module, 1.0/num) * Math.Cos(varphi/num), Math.Pow(module, 1.0 / num) * Math.Sin(varphi / num));
        }










        double Re
        {
            get
            {
                return real;
            }
            set
            {
                real = value;
            }
        }

        double Im
        {
            get
            {
                return imagine;
            }
            set
            {
                imagine = value;
            }
        }

        public static void onNullHandler()
        {
            Console.WriteLine("Divide by zero!");
            return;
        }
    }
}

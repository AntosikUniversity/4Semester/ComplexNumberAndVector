﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexNumberAndVector
{
    class Program
    {
        static void Main(string[] args)
        {
            ComplexNumber.onNull += ComplexNumber.onNullHandler;
            ComplexNumber n1 = new ComplexNumber(1, 1), n2 = new ComplexNumber(2, 2), nullNum = new ComplexNumber(0, 0);
            Console.WriteLine("n1 = {0}", n1);
            Console.WriteLine("n2 = {0}", n2);
            Console.WriteLine("nullNum = {0}", nullNum);
            Console.WriteLine("n1 + n2 = {0}", n1 + n2);
            Console.WriteLine("n1 - n2 = {0}", n1 - n2);
            Console.WriteLine("n1 * n2 = {0}", n1 * n2);
            Console.WriteLine("n1 / n2 = {0}", n1 / n2);
            Console.WriteLine("n1 / nullNum = {0}", n1 / nullNum);
            Console.WriteLine("|n1| = {0}", ComplexNumber.Module(n1));
            Console.WriteLine("n1 ^ 5 = {0}", ComplexNumber.Pow(n1, 5));
            Console.WriteLine("n1 ^ 1/5 = {0}", ComplexNumber.Root(n1, 5));

            List<ComplexNumber> list = new List<ComplexNumber>();
            list.Add(n1);
            list.Add(n2);

            Vector<ComplexNumber> first = new Vector<ComplexNumber>(list);
            Vector<ComplexNumber> second = new Vector<ComplexNumber>(list);
            Console.WriteLine("first = {0}", first);
            Console.WriteLine("second = {0}", second);
            Console.WriteLine("first + second = {0}", first + second);
            Console.WriteLine("first - second = {0}", first - second);

            List<Vector<ComplexNumber>> fororto = new List<Vector<ComplexNumber>>();
            fororto.Add(first);
            fororto.Add(second);
            Console.WriteLine("orto");
            List<Vector<ComplexNumber>> orto = (Vector<ComplexNumber>.Orto(fororto)).ToList<Vector<ComplexNumber>>();
            foreach(var vector in orto)
                Console.WriteLine(vector);

        }
    }
}
